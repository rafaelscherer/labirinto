const map = [
  'WWWWWWWWWWWWWWWWWWWWW',
  'W   W     W     W W W',
  'W W W WWW WWWWW W W W',
  'W W W   W     W W   W',
  'W WWWWWWW W WWW W W W',
  'W         W     W W W',
  'W WWW WWWWW WWWWW W W',
  'W W   W   W W     W W',
  'W WWWWW W W W WWW W F',
  'S     W W W W W W WWW',
  'WWWWW W W W W W W W W',
  'W     W W W   W W W W',
  'W WWWWWWW WWWWW W W W',
  'W       W       W   W',
  'WWWWWWWWWWWWWWWWWWWWW'
];


const lab = document.querySelector('.mapa');

function criarLab() {

  // i = linha
  // j = coluna

  for (let i = 0; i < map.length; i++) {
    let linha = document.createElement('div');
    linha.className = 'linha';

    for (let j = 0; j < map[i].length; j++) {
      let celula = i + '-' + j;

      if (map[i][j] == 'W') {
        let parede = document.createElement('div');
        parede.className = 'parede';
        parede.id = celula;
        linha.appendChild(parede);
      }

      if (map[i][j] == ' ') {
        let caminho = document.createElement('div');
        caminho.classList.add('caminho', 'lab');
        caminho.id = celula;
        linha.appendChild(caminho);
      }

      if (map[i][j] == 'S') {
        let inicio = document.createElement('div');
        inicio.classList.add('inicio', 'lab');
        inicio.id = celula;
        linha.appendChild(inicio);
      }

      if (map[i][j] == 'F') {
        let fim = document.createElement('div');
        fim.classList.add('fim', 'lab');
        fim.id = celula;
        linha.appendChild(fim);
      }
    }
    lab.appendChild(linha);
  }
}
criarLab();

let vertical = 9;
let horizontal = 0;

function createPlayer() {
  let player = document.createElement('div');
  player.id = 'player';
  player.style.width = '40px';
  player.style.height = '40px';
  player.style.backgroundImage = "url('./rato.jpg')";
  player.style.backgroundSize = 'cover';
  let celula = vertical + '-' + horizontal;
  let div = document.getElementById(celula);
  div.appendChild(player);
}
createPlayer();

function getPositionPlayer() {
  return document.getElementById("player").parentElement.id;
}

function getLine(positionPlayer) {
  let line = positionPlayer.split('-');
  // console.log(line);
  return parseInt(line[0]);
  
}

function getColumn(positionPlayer) {
  let column = positionPlayer.split('-');
  // console.log(column);
  return parseInt(column[1]);
}

function movePlayer(line, column) {
  let id = line + '-' + column;
  document.getElementById(id).appendChild(player);
}



document.addEventListener('keydown', (event) => {
  const keyName = event.key;
  let positionPlayer = getPositionPlayer();
  let line = getLine(positionPlayer);
  let column = getColumn(positionPlayer);

  if (keyName === "ArrowDown") {
    if (map[line + 1][column] === " ") {
      movePlayer(line + 1, column);
    }
  }

  if (keyName === "ArrowUp") {
    if (map[line - 1][column] === " ") {
      movePlayer(line - 1, column);
    }
  }

  if (keyName === "ArrowLeft") {
    if (map[line][column - 1] === " " || map[line][column - 1] === "S") {
      movePlayer(line, column - 1);
    }
  }

  if (keyName === "ArrowRight") {
    if (map[line][column + 1] === " ") {
      movePlayer(line, column + 1);
    }

    if (map[line][column + 1] === "F") {
      movePlayer(line, column + 1);
      alert("Voce venceu!");

    }
  }
});




